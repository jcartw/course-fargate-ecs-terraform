# course-fargate-ecs-terraform

## Commands

### 1-infrastructure

- init: `terraform init -backend-config="infrastructure-prod.config"`
- plan: `terraform plan -var-file="production.tfvars"`
- apply: `terraform apply -var-file="production.tfvars"`

### 2-platform

- init: `terraform init -backend-config="platform-prod.config"`
- plan: `terraform plan -var-file="production.tfvars"`
- apply: `terraform apply -var-file="production.tfvars"`

## Reference

- https://explore.skillbuilder.aws/learn/course/206/play/7823/subnets-gateways-and-route-tables-explained
