
# remote state
remote_state_key = "PROD/infrastructure.tfstate"
remote_state_bucket = "terraform-remote-state-4b80c3ef"

ecs_domain_name = "velleaccessories.com"
ecs_cluster_name = "Production-ECS-Cluster"
internet_cidr_blocks = "0.0.0.0/0"
